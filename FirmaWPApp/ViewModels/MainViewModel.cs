﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using FirmaWPApp.ServiceReference1;
using System.Linq;

namespace FirmaWPApp.ServiceReference1
{
    public partial class Order
    {
        public Customer Customer
        {
            get { return App.ViewModel.Customers.FirstOrDefault(i => i.CustomerID == CustomerID); }
            set { CustomerID = value.CustomerID; }
        }
    }

}

namespace FirmaWPApp
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Orders = new ObservableCollection<Order>();
            this.Customers = new ObservableCollection<Customer>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<Order> Orders { get; private set; }
        public ObservableCollection<Customer> Customers { get; private set; }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            var client = new Service1Client();

            if(Customers.Count == 0)
            {
                client.GetCustomersCompleted += new EventHandler<GetCustomersCompletedEventArgs>(client_GetCustomersCompleted);
                client.GetCustomersAsync();
            }

            client.LatestOrdersCompleted += (sender, args) =>
                                                {
                                                    Orders.Clear();
                                                    args.Result.ForEach(i=> Orders.Add(i));
                                                };
            client.LatestOrdersAsync(new DateTime(2000,1,1));

            this.IsDataLoaded = true;
        }

        void client_GetCustomersCompleted(object sender, GetCustomersCompletedEventArgs e)
        {
            Customers.Clear();
            e.Result.ForEach(i=>Customers.Add(i));
            NotifyPropertyChanged("Orders");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}