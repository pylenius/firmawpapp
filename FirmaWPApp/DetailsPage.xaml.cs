﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using FirmaWPApp.ServiceReference1;

namespace FirmaWPApp
{
    public partial class DetailsPage : PhoneApplicationPage
    {
        // Constructor
        public DetailsPage()
        {
            InitializeComponent();
            customerSelect.ItemsSource = App.ViewModel.Customers;
        }

        // When page is navigated to set data context to selected item in list
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if(!App.ViewModel.IsDataLoaded)
                App.ViewModel.LoadData();

            if (DataContext != null)
                return;

            string selectedNumber = "";
            if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedNumber))
            {
                var number = int.Parse(selectedNumber);
                var service = new Service1Client();
                service.GetOrderCompleted += (sender, args) => { DataContext = args.Result; };
                service.GetOrderAsync(number);
            }
            else
            {
                DataContext = new Order {Date = DateTime.Now};
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            var service = new Service1Client();
            service.SaveOrderCompleted += (o, args) =>
                                          Deployment.Current.Dispatcher.BeginInvoke(() =>
                                             NavigationService.Navigate(
                                                 new Uri("/MainPage.xaml", UriKind.Relative)));
            service.SaveOrderAsync(DataContext as Order);
        }
    }
}