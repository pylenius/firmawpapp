﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FirmaWPApp.ServiceReference2;
using Microsoft.Phone.Controls;
using FirmaWPApp.ServiceReference1;
using Microsoft.Phone.Notification;

namespace FirmaWPApp
{
    public partial class MainPage : PhoneApplicationPage
    {

        //Data members for this class

        //A unique ID for your phone, saved to internal memory
        private Guid deviceID;

        //Add a reference to your deployed Push Service URL and name it ServiceReference1 and this line will compile.
        //In Solution Explorer, Right-click References, then click "Add Service Reference," then enter the URL for the SVC file 
        //for the Push Service and click "Go." Verify that the Push Service's four methods are indeed exposed then click "OK." 
        private PushServiceClient myCLient = new PushServiceClient();

        //URI for this app's notification channel.
        private HttpNotificationChannel myPushChannel;


        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);



            //First, generate a GUID on this phone and save it; reuse if it exists to keep consistent with service's data.
            if (IsolatedStorageSettings.ApplicationSettings.Contains("DeviceId"))
            {
                //load existing
                deviceID = (Guid)IsolatedStorageSettings.ApplicationSettings["DeviceId"];
            }
            else
            {
                //generate new ID
                deviceID = Guid.NewGuid();
                IsolatedStorageSettings.ApplicationSettings["DeviceId"] = deviceID;
            }

            //Next, create a notification channel which will have its own URI that the web service will need to know.
            //We're naming the channel for this app "myChannel." Channels need only be created once per app; if the URI
            //ever changes, the ChannelUriUpdated event will fire. This event also fires when the channel is created for 
            //the first time.
            myPushChannel = HttpNotificationChannel.Find("myChannel");

            //check to see if this channel has already been created; if so, reuse
            if (myPushChannel == null)
            {
                //rawmessage.Text += " Channel was null.";
                //This channel needs to be created. ChannelUriUpdated will fire upon completion.
                myPushChannel = new HttpNotificationChannel("myChannel","PYService");
                attachHandlerFunctions();
                myPushChannel.Open();
            }
            else
            {
                //ChannelUriUpdated is not going to fire. Call subscribing method on web service. Attach delegates.
                attachHandlerFunctions();
                myCLient.SubscribeMyPhoneAsync(deviceID,myPushChannel.ChannelUri.ToString());
            }

        }

        private void attachHandlerFunctions()
        {
            //attaches delegates to push channel events

            //if error, print onscreen
            myPushChannel.ErrorOccurred += (sender, args) =>
                                           Deployment.Current.Dispatcher.BeginInvoke(() =>
                                                                                         {
                                                                                             MessageBox.Show(
                                                                                                 args.Message,
                                                                                                 "pushError",
                                                                                                 MessageBoxButton.OK);
                                                                                         });

            myPushChannel.ChannelUriUpdated +=
                new EventHandler<NotificationChannelUriEventArgs>(myPushChannel_ChannelUriUpdated);

            //Handle raw push notifications, which are received only while app is running.
            myPushChannel.HttpNotificationReceived += new EventHandler<HttpNotificationEventArgs>(myPushChannel_HttpNotificationReceived);

            // Toast push message
            myPushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(myPushChannel_ShellToastNotificationReceived);
        }

        void myPushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            string uri;
            if (e.Collection.TryGetValue("wp:Param", out uri))
            {
                Deployment.Current.Dispatcher.BeginInvoke(
                    () =>
                        {
                            var result = MessageBox.Show(e.Collection["wp:Text2"],
                                                         e.Collection["wp:Text1"],
                                                         MessageBoxButton.OKCancel);
                            App.ViewModel.LoadData();
                            if (result == MessageBoxResult.OK)
                                NavigationService.Navigate(new Uri(uri, UriKind.Relative));

                        });

            }            
        }

        void myPushChannel_HttpNotificationReceived(object sender, HttpNotificationEventArgs e)
        {
            
        }

        void myPushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            //Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show(e.ChannelUri.ToString(), "ChannelUriUpdated", MessageBoxButton.OK));
            //also fires in notification thread. There are a couple things we want to do here, once we know we 
            //have a valid channel from MPNS. 
            // 1) Make sure phone's shell knows this channel is authorized to receive Tile updates when app is not running.
            if (myPushChannel.IsShellTileBound == false)
            {
                var ListOfAllowedDomains = new Collection<Uri>
                                               {
                                                   new Uri("http://90.fi")
                                               };

                //Register this channel with the shell, pass on authorized domain in way method expects
                myPushChannel.BindToShellTile(ListOfAllowedDomains);
            }

            //2) Make sure phone's shell knows this channel is authorized to receive Toast messages when app is not running
            if (myPushChannel.IsShellToastBound == false)
            {
                myPushChannel.BindToShellToast();
            }

            //3) Show that this event fired onscreen.
            //Deployment.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    MessageBox.Show("uri updated");
            //});

            //4) Pass the new ChannelUri on to the subscription service.
            myCLient.SubscribeMyPhoneAsync(deviceID,e.ChannelUri.ToString());
        }

        // Handle selection changed on ListBox
        private void MainListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If selected index is -1 (no selection) do nothing
            if (MainListBox.SelectedIndex == -1)
                return;

            // Navigate to the new page
            NavigationService.Navigate(new Uri("/DetailsPage.xaml?selectedItem=" + (MainListBox.SelectedItem as Order).Number, UriKind.Relative));

            // Reset selected index to -1 (no selection)
            MainListBox.SelectedIndex = -1;
        }

        // Load data for the ViewModel Items
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/DetailsPage.xaml", UriKind.Relative));
        }
    }
}